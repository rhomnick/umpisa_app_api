# Umpisa Inc API

## Clone the repository


```
$ git clone git clone https://bitbucket.org/rhomnick/umpisa_app_api.git
```
```
$ cd umpisa_app_api
```
```
$ npm install
```

**Before starting the api server, let's run a mongodb in you local Docker**
```
$ cd mongo/
```
```
$ docker-compose up -d
```
**Seed database**
This is needed to populate inital datas to the database

```
$ npm run seed
```
**now we can start our api server**
```
$ npm start
```

**Dockerizing the app**
Should you need to dockerize the app, a Dockerfile is included in the root folder

### Enjoy!