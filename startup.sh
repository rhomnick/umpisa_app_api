#!/bin/bash

export HOST="0.0.0.0"

case $1 in 
  dev)
    export NODE_ENV="development"
    export PORT=9000 && \
    export MONGO_URL="mongodb://localhost:27016/umpisa"
    export ENCRYPT_KEY="3d^19&8f*s2d34fh_4&82@s7h9%*8fh0ad&*"
    nodemon index.js;;
  prod)
    export NODE_ENV="production"
    export PORT=9000 && \
    export MONGO_URL="mongodb://localhost:27016/umpisa"
    export ENCRYPT_KEY="3d^19&8f*s2d34fh_4&82@s7h9%*8fh0ad&*"
    node index.js;;
  *)
    echo $"Usage: $0 { dev | test | prod }"
    exit 1
esac
