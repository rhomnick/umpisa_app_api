const config = {
  mongo: {
    url: process.env.MONGO_URL || 'mongodb://localhost:27017/dev',
    options: {
      useNewUrlParser: true,
      poolSize: 10,
      autoReconnect: true,
      reconnectInterval: 500,
      reconnectTries: Number.MAX_VALUE
    }
  },
  k8s: {
    url: process.env.MONGO_URL || 'mongodb://localhost:27017/dev',
    options: {
      poolSize: 10,
      autoReconnect: true,
      reconnectInterval: 500,
      reconnectTries: Number.MAX_VALUE,
      useNewUrlParser: true,
      replicaSet: 'CIERGIO_RS_0'
    }
  },
  host: process.env.HOST || '0.0.0.0',
  port: process.env.PORT || 4000,
  amqpURI: process.env.AMQP_URI || 'amqp://localhost:5673',
  queuesSettings: {
    durable: true,
    noAck: false
  },
  cors: {
    origin: '*',
    methods: 'GET,PUT,POST,DELETE',
    headers: 'Content-Type,slave,spoil,treat,jrr',
    maxAge: 5000,
    exposedHeaders: 'slave,spoil,treat,jrr',
    credentials: false
  },
  jwt: {
    algorithm: 'HS256',
    key: process.env.ENCRYPT_KEY || '3d^19&8f*s2d34fh_4&82@s7h9%*8fh0ad&*'
  },
  encrypt: {
    salt: 10,
    algorithm: 'sha256',
    key: process.env.ENCRYPT_KEY || '3d^19&8f*s2d34fh_4&82@s7h9%*8fh0ad&*'
  },
  sendGrid: {
    emailVerificationLink: process.env.EMAIL_VERIFICATION_LINK,
    resetPasswordRedirectLink: process.env.RESET_PASSWORD_LINK,
    apiKey:
      process.env.SENDGRID_APIKEY ||
      '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'
  },
  emailVerificationRedirectLink: process.env.EMAIL_VERIFICATION_REDIRECT_LINK
};

module.exports = config;
