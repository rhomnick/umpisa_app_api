const ResolverError = require('./ResolverError');
const errorCodes = require('./errorCodes');
const httpStatus = require('http-status-codes');

module.exports = (log, schema, data) => {
  const validationResult = schema(data);

  if (validationResult !== true) {
    const messageArray = validationResult.map(result => result.message);
    const message = messageArray.join(',');

    log.error(`${message}`);
    throw new ResolverError(message, 1000, httpStatus.BAD_REQUEST);
  }
};
