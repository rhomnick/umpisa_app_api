const { Types } = require('mongoose');

// Converts the 24digit hex string to objectId and then catch if input is not valid 24hex
// This checks if the property exist
// If the property exist, check if the value is 24digit hex
// If true make the value a real ObjectId
// Else this will invalidate the field by returning false
// This will return the whole object with the updated objectId fields
// NOTE: This doesnt accepts an array props[key]


module.exports  = (objectName, props, logger) => {

    if(objectName) {

        for(key in props ) {

            if(!objectName[props[key]] && objectName[props[key]]!=="") {
    
                delete objectName[props[key]];
                
            } else {
    
                if(!Types.ObjectId.isValid(objectName[props[key]])) {
                    if(logger) {
                        const err = `${props[key]} must be a valid ObjectID`;
                        logger.error(err)
                        throw err;
                    }
                    return false
                } else {
                    objectName[props[key]] = Types.ObjectId(objectName[props[key]])
                }
            }
        }
    }
    return objectName;
    
}

