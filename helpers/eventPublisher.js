module.exports = amqp => {
  class EventPublisher {
    constructor() {
      this.events = [];
    }

    async publish() {
      try {
        this.events.forEach(async event => {
          await amqp.send('EventStore', event, {
            durable: true,
            persistent: true
          });
        });
      } catch (error) {
        throw error;
      }
    }

    add(event) {
      this.events.push(event);
    }
  }

  return EventPublisher;
};
