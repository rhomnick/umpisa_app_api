
const securePin = require('secure-pin');
const charSet = new securePin.CharSet();

charSet
    .addUpperCaseAlpha()
    .addNumeric()
    .randomize();

const generate = (digit) => {
    return securePin.generateStringSync(digit, charSet);
};

module.exports = {
    generate
};
