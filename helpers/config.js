module.exports = {
  logger: {
    levels: ['start', 'info', 'warn', 'error', 'success', 'data', 'header'],
    colors: {
      start: 'cyan',
      info: 'green',
      warn: 'yellow',
      error: 'red',
      debug: 'blue',
      success: 'cyan',
      data: 'magenta',
      header: 'gray'
    },
    consoleColor: 'reset',
    showLevels: true,
    levelsCase: 'uppercase',
    colorizeLevels: false
  }
};
