const { key, algorithm } = require('../config').jwt;
const jwt = require('jsonwebtoken');

module.exports = {
  sign: async payload => {
    return await jwt.sign(payload, key, { algorithm });
  },
  verify: async token => {
    return await jwt.verify(token, key);
  }
};
