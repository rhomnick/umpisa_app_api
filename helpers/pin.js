const securePin = require('secure-pin');

const charSet = new securePin.CharSet();
charSet
  .addUpperCaseAlpha()
  .addNumeric()
  .randomize();

const generate = () => {
  return securePin.generateStringSync(6, charSet);
};

module.exports = { generate };
