const puid = require('./puid');
const httpStatus = require('http-status-codes');
const ResolverError = require('./ResolverError');
const Log = require('./log');
const errorCodes = require('./errorCodes');
const Auth = require('../graphql/auth')

module.exports = amqp => {
  return {
    requiredHeaders(next, source, args, ctx, info) {
      try {
        const { headers } = ctx;

        if (!headers.spoil) {
          throw new ResolverError(errorCodes[2000], 20001, httpStatus.UNAUTHORIZED);
        }

        if (!headers.treat) {
          throw new ResolverError(errorCodes[2000], 20002, httpStatus.UNAUTHORIZED);
        }

        ctx.processId = puid.generate();

        return next();
      } catch (error) {
        // const logger = new Log(ctx.processId, info.fieldName);
        // logger.header(ctx.req.headers);
        // logger.error(error);

        if (error.code) {
          ctx.res.status(error.status);
          throw error;
        } else {
          ctx.res.status(httpStatus.INTERNAL_SERVER_ERROR);
          throw new ResolverError(error.message, 3000);
        }
      }
    },

    async auth(next, source, args, ctx, info) {
      try {
        const { slave, spoil, treat } = ctx.headers;

        if (!slave) {
          throw new ResolverError(errorCodes[2000], 20003, httpStatus.UNAUTHORIZED);
        }

        const authResponse = await Auth.verify({
          processId: ctx.processId,
          token: slave,
          fieldName: info.fieldName,
          spoil,
          device: treat
        });

        if (authResponse.error) {
          throw new ResolverError(errorCodes[2000], 20004, httpStatus.UNAUTHORIZED);
        }

        ctx.user = { ...authResponse.user, id: authResponse.user._id };

        return next();
      } catch (error) {
        // const logger = new Log(ctx.processId, info.fieldName);
        // logger.header(ctx.req.headers);
        // logger.error(error);

        if (error.code) {
          ctx.res.status(error.status);
          throw error;
        } else {
          ctx.res.status(httpStatus.INTERNAL_SERVER_ERROR);
          throw new ResolverError(error.message, 3000);
        }
      }
    }
  };
};
