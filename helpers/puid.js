const uniqid = require('uniqid');

module.exports = {
  generate: () => {
    return uniqid();
  }
};
