const moment = require('moment');
const config = require('./config');

const Logger = require('colorize-log');
const logger = new Logger(config.logger);

const puid = require('./puid');

class Log {
  constructor(processId, processName) {
    if (!processId) {
      this.processId = puid.generate();
    }
    this.processId = processId;
    this.processName = processName;
    this.date = moment().format('MMMM Do YYYY, h:mm:ss a');
  }

  start() {
    console.log('\n\n');
    logger.start(
      `PID = ${this.processId} Date = ${this.date} -> ${this.processName} | STARTED`
    );
  }

  info(message) {
    logger.info(
      `PID: ${this.processId} PDate = ${this.date} -> ${this.processName} | ${message}`
    );
  }

  warn(message) {
    logger.warn(
      `PID = ${this.processId} Date = ${this.date} -> ${this.processName} | ${message}`
    );
  }

  debug(message) {
    logger.debug(
      `PID = ${this.processId} Date = ${this.date} -> ${this.processName} | ${message}`
    );
  }

  error(error) {
    logger.error(`PID = ${this.processId} Date = ${this.date} -> ${this.processName} | `);
    logger.error(error);
  }

  success(message) {
    if (message) {
      logger.success(
        `PID = ${this.processId} Date = ${this.date} -> ${
          this.processName
        } | ${message} | SUCCESS`
      );
    } else {
      logger.success(
        `PID = ${this.processId} Date = ${this.date} -> ${this.processName} | SUCCESS`
      );
    }
  }

  data(message) {
    logger.data(
      `PID = ${this.processId} Date = ${this.date} -> ${
        this.processName
      } | ${JSON.stringify(message)}`
    );
  }

  header(headers) {
    for (let key in headers)
      logger.header(
        `PID = ${this.processId} Date = ${this.date} -> ${this.processName} | ${key}: ${
          headers[key]
        }`
      );
  }

  init(headers, data) {
    this.start();
    this.header(headers);
    this.data(data);
  }
}

module.exports = Log;
