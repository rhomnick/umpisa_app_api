const Log = require('./log');
const puid = require('./puid');
const ResolverError = require('./ResolverError');
const eventPublisher = require('./eventPublisher');
const directives = require('./directives');
const validate = require('./validate');
const errorCodes = require('./errorCodes');

module.exports = amqp => {
  return {
    Log,
    puid,
    ResolverError,
    EventPublisher: eventPublisher(amqp),
    directives: directives(amqp),
    validate,
    errorCodes
  };
};
