const { salt } = require('../config').encrypt;
const bcrypt = require('bcryptjs');

const encrypt = password => {
  return bcrypt.hashSync(password, salt);
};

const compare = (password, hashedPassword) => {
  return bcrypt.compareSync(password, hashedPassword);
};

module.exports = {
  encrypt,
  compare
};
