const config = require('../config');

const sendGrid = require('@sendgrid/mail');
sendGrid.setApiKey(config.sendGrid.apiKey);

module.exports = sendGrid;
