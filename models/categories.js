const mongoose = require('mongoose');
const { Schema } = mongoose;
const { Types } = Schema;
const { status } = require('../helpers/constants')

const schema = new Schema(
  {
    authorAccountId: Types.ObjectId,
    name: String,
    typeId: Types.ObjectId,
    status: {
      type: String,
      enum: [
        status.records.ACTIVE,
        status.records.DELETED,
        status.records.UNPUBLISHED
      ],
      default: 'active'
    },
  },
  { collection: 'Categories', timestamps: true }
);

mongoose.model('Categories', schema);
