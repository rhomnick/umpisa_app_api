const mongoose = require('mongoose');
const { Schema } = mongoose;

const SettingSchema = new Schema(
  {
    loggedDeviceLimit: { type: Number, default: null }
  },
  { collection: 'Settings', timestamps: true }
);

mongoose.model('Setting', SettingSchema);
