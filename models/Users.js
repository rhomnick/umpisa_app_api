const mongoose = require('mongoose');
const { Schema } = mongoose;

const UserSchema = new Schema(
  {
    firstName: { type: String, default: null },
    lastName: { type: String, default: null },
    avatar: { type: String, default: null },
    email: { type: String, default: null },
    password: { type: String, default: null },
    userType: { type: String, default: null },
    status: { type: String, default: 'active' }
  },
  { collection: 'Users', timestamps: true }
);

mongoose.model('Users', UserSchema);
