const mongoose = require('mongoose');
const { Schema } = mongoose;
const { Types } = Schema;
const { status } = require('../helpers/constants')

const Records = new Schema(
  {
    authorId: Types.ObjectId,
    typeId: Types.ObjectId,
    categoryId: Types.ObjectId,
    title: Types.String,
    typeLiteral: Types.String,
    primaryImage: Types.String,
    content: Types.String,
    status: {
      type: String,
      enum: [
        status.records.ACTIVE,
        status.records.UNPUBLISHED,
        status.records.TRASHED,
        status.records.DELETED,
      ],
      default: 'active'
    },
    publishedAt: { type: Date, default: new Date() },
  },
  { collection: 'Records', timestamps: true }
);

mongoose.model('Records', Records);
