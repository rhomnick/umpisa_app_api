const mongoose = require('mongoose');
const { Schema } = mongoose;

const SessionSchema = new Schema(
  {
    user: {
      _id: Schema.Types.ObjectId,
      email: { type: String },
      roleId: Schema.Types.ObjectId
    },
    device: { type: String },
    token: { type: String }
  },
  { collection: 'Sessions', timestamps: true }
);

mongoose.model('Session', SessionSchema);
