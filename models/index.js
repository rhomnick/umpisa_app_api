
const mongoose = require('mongoose');
const env = process.env.NODE_ENV || 'development';

if (env !== 'k8s') {
  const { mongo } = require('../config');
  mongoose.connect(
    mongo.url,
    mongo.options
  );
} else {
  const { k8s } = require('../config');
  mongoose.connect(
    k8s.url,
    k8s.options
  );
}

require('./types');
require('./categories');
require('./records');
require('./notes');
require('./Session');
require('./Settings');
require('./Users');
