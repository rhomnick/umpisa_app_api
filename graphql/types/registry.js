const { status } = require('../../helpers/constants');

module.exports = `

  # ____ TYPES _____
  
  type Type {
    _id: String
    name: String
  }
  
  # ____ CATEGORIES _____

  type Category {
    _id: String
    name: String
    status: String
    author: User
    type: Type
    createdAt: String
  }
  
  input CategoriesInput{
    typeId: String
    status: String
  }
  
  type CategoriesResponse {
    data: [Category]
    limit: Int
    offset: Int
    count: Int
  }
  
# ____ RECORDS _____

  input SingleRecordInput {
    _id: String
  }

  input RecordsInput{
    typeId: String
    categoryId: String
    typeLiteral: String
  }

  enum RecordStatus {
    ${status.records.ACTIVE}
    ${status.records.TRASHED}
    ${status.records.DELETED}
    ${status.records.UNPUBLISHED}
  }

  type SingleRecord {
    _id: String
    author: User
    type: Type
    category: Category
    title: String
    primaryImage: String
    content: String
    status: String
  }

  type ListingResponse {
    data: [SingleRecord]
    limit: Int
    offset: Int
    count: Int
  }

  type History {
    _id: String
    author: User
    activity: String
    updatedAt: String
  }

  type HistoryResponse {
    data: [History]
    limit: Int
    offset: Int
    count: Int
  }

  type Query {

    getCategories(
        where: CategoriesInput
        limit: Int
        offset: Int 
      ) : CategoriesResponse @auth @requiredHeaders

    getRecords(
        where: RecordsInput
        limit: Int
        offset: Int 
      ) : ListingResponse @auth @requiredHeaders

      getRecord(
        recordId:String
      ): SingleRecord @auth @requiredHeaders

    getRecordHistory(
      recordId: String
      limit: Int
      offset: Int 
      ): HistoryResponse @auth @requiredHeaders
  }

`;
