const { status } = require('../../helpers/constants')

module.exports = `

  input CreateTypeInput {
    name: String
    status: TypeStatus
  }
  input UpdateTypeInput {
    name: String
    status: TypeStatus
  }

  input CreateInput {
    authorId: String
    name: String
    status: TypeStatus
  }

  enum TypeStatus {
    ${status.records.ACTIVE}
    ${status.records.DELETED}
    ${status.records.UNPUBLISHED}
  }

  type Mutation {
    createType(
        data: CreateTypeInput
      ) : Response @auth @requiredHeaders
    updateType(
        id: String
        data: CreateTypeInput
      ) : Response @auth @requiredHeaders

    createRegistry(data: CreateInput) : Response @auth @requiredHeaders
  }
  
`;
