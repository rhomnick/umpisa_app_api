const { status } = require('../../helpers/constants')

module.exports = `

  input CreateRecordInput {
    typeId: String
    typeLiteral: String
    categoryId: String
    title: String
    primaryImage: String
    content: String
  }
  input UpdateRecordInput {
    status: RecordStatus
    typeId: String
    categoryId: String
    title: String
    primaryImage: String
    content: String
  }

  type Mutation {
    createRecord(
        data: CreateRecordInput
        note: String
      ) : SingleRecord @auth @requiredHeaders
    updateRecord(
        id: String
        data: CreateRecordInput
      ) : SingleRecord @auth @requiredHeaders
  }
  
`;
