const { status } = require('../../helpers/constants')

module.exports = `

  input CreateCategoryInput {
    name: String
    status: CategoryStatus
    typeId: String
  }
  input UpdateCategoryInput {
    name: String
    status: CategoryStatus
    typeId: String
  }
  
  enum CategoryStatus {
    ${status.records.ACTIVE}
    ${status.records.DELETED}
    ${status.records.UNPUBLISHED}
  }

  type Mutation {
    createCategory(
        data: CreateCategoryInput
      ) : Response @auth @requiredHeaders
    updateCategory(
        id: String
        data: CreateCategoryInput
      ) : Response @auth @requiredHeaders
  }
  
`;
