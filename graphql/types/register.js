module.exports = `

type RegisterResponse {
  _id: String
  processId: String
  message: String
  slave: String
}

  input InputUser {
    avatar: String
    firstName: String
    lastName: String
    email: String
    password: String
  }
  
  type Mutation {

    # Register a user based on the code provided
    
    register(
      
      """ InputUser type, required """
      data: InputUser
      
      """ code from the verification email, required """
      code: String
    
      ): RegisterResponse @requiredHeaders
  }
`