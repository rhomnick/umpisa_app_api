module.exports = `
  input InputLogout {
    notificationToken: String
  }

  type Mutation {
    # Validates user's session then sends a request to authentication service to remove the user's session 
    logout(data: InputLogout): Response @auth @requiredHeaders
  }
`;
