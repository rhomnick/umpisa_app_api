const { status } = require('../../helpers/constants')

module.exports = `
  input CreateNoteInput {
    content: String
    recordId: String
  }

  input UpdateNoteInput {
    recordId: String
    content: String
    status: NoteStatus
  }


  enum NoteStatus {
    ${status.records.ACTIVE}
    ${status.records.DELETED}
    ${status.records.UNPUBLISHED}
  }

  type Mutation {
    createNote(
        data: CreateNoteInput
      ) : Response @auth @requiredHeaders
    updateNote(
        id: String
        data: UpdateNoteInput
      ) : Response @auth @requiredHeaders
  }
  
`;
