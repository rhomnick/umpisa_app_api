module.exports = `
  directive @requiredHeaders on QUERY | FIELD_DEFINITION
  directive @auth on QUERY | FIELD_DEFINITION
`;