module.exports = `

  type Response {
    _id: String
    processId: String
    message: String
  }

  type ResponseArray {
    _id: [String]
    processId: String
    message: String
  }

  type IssueResponse {
    _id: String
    code: String
    processId: String
    message: String
  }
`;
