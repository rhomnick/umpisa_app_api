module.exports = `
  
  type User{
    _id: String,
    firstName: String,
    lastName: String,
    avatar: String,
    email: String,
    status: String
    analytics: PostAnalytics
  }
  type PostAnalytics {
    posts: Int
    products: Int
    todos: Int
  }
  type LoginResponse {
    user: User
    slave: String
  }

  type Query {
    profile : User @auth @requiredHeaders
  }

  type Mutation {
    
    login(
      
      """ email address, required """
      email: String 
      
      """ password, required """
      password: String
    
      ): LoginResponse @requiredHeaders
  }
`