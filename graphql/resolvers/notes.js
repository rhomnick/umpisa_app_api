const httpStatus = require('http-status-codes');
const { Log, validate, ResolverError, errorCodes, encrypt, constants } = require('../../helpers');
const { events, service, status } = constants;

const Mutation = {

  async createNote(_, args, ctx, info) {
    const log = new Log(ctx.processId, info.fieldName);
    log.init(ctx.headers, args);

    try {
      const { Notes, Records } = ctx.models;
      const { CreateNoteSchema } = ctx.validators;

      validate(log, CreateNoteSchema, args.data);

      args.data.authorId = ctx.user._id;

      const exist = await Records.findOne({ _id: args.data.recordId });
      if (!exist)
        throw new ResolverError(' record does note exist.', 5000, httpStatus.NOT_FOUND)

      const result = await new Notes(args.data).save();
      log.info('Creating Note -> [OK]')

      // ***** Response Data
      const responseObj = {
        _id: result._id,
        message: 'success',
        processId: ctx.processId
      };
      log.success(JSON.stringify(responseObj));
      return responseObj;

    } catch (error) {
      if (error.code) {
        ctx.res.status(error.status);
        throw error;
      } else {
        ctx.res.status(httpStatus.INTERNAL_SERVER_ERROR);
        log.error(error);
        throw new ResolverError(errorCodes[3000] + ` (${ctx.processId})`, 3000);
      }
    }
  },

  async updateNote(_, args, ctx, info) {
    const log = new Log(ctx.processId, info.fieldName);
    log.init(ctx.headers, args);

    try {
      const { Notes } = ctx.models;
      const { UpdateNoteSchema } = ctx.validators;

      validate(log, UpdateNoteSchema, args);

      const result = await Notes.findOne({ _id: args.id });
      if (!result)
        throw new ResolverError(' note not found.', 5000, httpStatus.NOT_FOUND)


      // ***** Update Data
      await result.set(args.data);
      await result.save();
      log.info('Updating Note -> [OK]')
      // ***** Response Data
      const responseObj = {
        _id: result._id,
        message: 'success',
        processId: ctx.processId
      };
      log.success(JSON.stringify(responseObj));
      return responseObj;

    } catch (error) {
      if (error.code) {
        ctx.res.status(error.status);
        throw error;
      } else {
        ctx.res.status(httpStatus.INTERNAL_SERVER_ERROR);
        log.error(error);
        throw new ResolverError(errorCodes[3000] + ` (${ctx.processId})`, 3000);
      }
    }
  },

};

module.exports = {
  Mutation
};
