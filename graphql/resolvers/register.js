const { Log, validate, ResolverError, errorCodes, encrypt, amqp, constants, loki, pin } = require('../../helpers');
const httpStatus = require('http-status-codes');
const Auth = require('../auth')

const Mutation = {
  async register(_, args, ctx, info) {
    const log = new Log(ctx.processId, info.fieldName);
    log.init(ctx.headers, args);

    try {
      const { validators, models } = ctx;
      const { Users } = models;
      const { RegisterSchema } = validators;

      validate(log, RegisterSchema, args);

      const data = args.data;
      const user = await Users.create({
        ...data,
        password: encrypt.encrypt(data.password),
      }).then(user => {
        return user
      });

      const token = await Auth.sign({
        processId: log.processId,
        user: {
          _id: user._id,
          email: user.email,
        },
        device: ctx.headers.treat,
        spoil: ctx.headers.spoil
      }).then(result => {
        if (result.error) {
          const error = result.error;
          throw new ResolverError(error.message, error.code, httpStatus.UNAUTHORIZED);
        }
        return result.token;
      });

      ctx.res.set('slave', token);

      const responseObj = {
        _id: user._id,
        message: 'success',
        processId: log.processId,
        slave: token
      };

      log.success(JSON.stringify(responseObj));

      return responseObj;
    } catch (error) {
      if (error.code) {
        log.error(error.message);
        ctx.res.status(error.status);
        throw error;
      } else {
        ctx.res.status(httpStatus.INTERNAL_SERVER_ERROR);
        log.error(error.message);
        throw new ResolverError(errorCodes[3000], 3000);
      }
    }
  }
};

module.exports = {
  Mutation
};
