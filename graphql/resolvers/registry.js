const { toObjectId, Log, puid, constants } = require('../../helpers');
const { status, queues, exchange } = constants;



const Query = {

  async getCategories(_, args, ctx, info) {
    const logger = new Log(ctx.processId, info.fieldName);

    const { where, limit, offset } = args
    const { Categories } = ctx.models;
    let params = { ...args.where }
    toObjectId(params, ['typeId'], logger);
    let query = [
      { $match: params },
      { $lookup: { from: 'Users', localField: 'authorId', foreignField: '_id', as: 'author' } },
      { $unwind: { path: '$author', preserveNullAndEmptyArrays: true } },
      { $lookup: { from: 'Types', localField: 'typeId', foreignField: '_id', as: 'type' } },
      { $unwind: { path: '$type', preserveNullAndEmptyArrays: true } },
    ]
    query.push({ $group: { _id: null, data: { $push: '$$ROOT' }, count: { $sum: 1 } } })
    query.push({
      $project: {
        data: { $slice: ['$data', offset, limit] },
        count: '$count',
        limit: { $literal: limit },
        offset: { $literal: offset }
      }
    })
    let data = await Categories.aggregate(query).allowDiskUse(true);
    return data[0]
  },

  async getRecords(_, args, ctx, info) {
    const logger = new Log(ctx.processId, info.fieldName);

    const { where, limit, offset } = args
    const { Records, User, Building, Complex, Company } = ctx.models;
    let params = { ...args.where }
    toObjectId(params, ['typeId', 'categoryId', 'companyId', 'complexId', 'buildingId'], logger);
    let query = [
      { $match: params },
      { $lookup: { from: 'Users', localField: 'authorId', foreignField: '_id', as: 'author' } },
      { $unwind: { path: '$author', preserveNullAndEmptyArrays: true } },
      { $lookup: { from: 'Types', localField: 'typeId', foreignField: '_id', as: 'type' } },
      { $unwind: { path: '$type', preserveNullAndEmptyArrays: true } },
      { $lookup: { from: 'Categories', localField: 'categoryId', foreignField: '_id', as: 'category' } },
      { $unwind: { path: '$category', preserveNullAndEmptyArrays: true } },
    ]
    query.push({ $group: { _id: null, data: { $push: '$$ROOT' }, count: { $sum: 1 } } })
    query.push({
      $project: {
        data: { $slice: ['$data', offset, limit] },
        count: '$count',
        limit: { $literal: limit },
        offset: { $literal: offset }
      }
    })
    let data = await Records.aggregate(query).allowDiskUse(true);
    return data[0]
  },

  async getRecord(_, args, ctx, info) {
    const logger = new Log(ctx.processId, info.fieldName);

    const { recordId } = args
    const { Records } = ctx.models;
    let params = { _id: recordId }
    toObjectId(params, ['_id'], logger);
    let query = [
      { $match: params },
      { $lookup: { from: 'Users', localField: 'authorId', foreignField: '_id', as: 'author' } },
      { $unwind: { path: '$author', preserveNullAndEmptyArrays: true } },
      { $lookup: { from: 'Types', localField: 'typeId', foreignField: '_id', as: 'type' } },
      { $unwind: { path: '$type', preserveNullAndEmptyArrays: true } },
      { $lookup: { from: 'Categories', localField: 'categoryId', foreignField: '_id', as: 'category' } },
      { $unwind: { path: '$category', preserveNullAndEmptyArrays: true } },
    ]
    let data = await Records.aggregate(query).allowDiskUse(true);
    return data[0]
  },

  async getRecordHistory(_, args, ctx, info) {
    const logger = new Log(ctx.processId, info.fieldName);
    try {
      const offset = args.offset || 0;
      const limit = args.limit || 10;
      const nulldata = { data: [], limit, offset, count: 0 };
      const { AuditLog } = ctx.models;

      logger.init(ctx.headers, { args, limit, offset });
      toObjectId(args, ['recordId'], logger);

      const result = await AuditLog.aggregate([
        { $match: { uuid: args.recordId, service: 'Registry', action: { $in: ['REGISTRY_RECORD_UPDATED', 'REGISTRY_RECORD_CREATED'] } } },
        { $lookup: { from: 'Users', localField: 'authorId', foreignField: '_id', as: 'author' } },
        { $unwind: { path: '$author', preserveNullAndEmptyArrays: true } },
        { $lookup: { from: 'Types', localField: 'typeId', foreignField: '_id', as: 'type' } },
        { $unwind: { path: '$type', preserveNullAndEmptyArrays: true } },
        { $lookup: { from: 'Categories', localField: 'categoryId', foreignField: '_id', as: 'category' } },
        { $unwind: { path: '$category', preserveNullAndEmptyArrays: true } },
        { $sort: { createdAt: -1 } },
        { $project: { author: 1, account: 1, changes: 1, action: 1, type: 1, updatedAt: '$createdAt' } },
        { $group: { _id: null, data: { $push: '$$ROOT' }, count: { $sum: 1 } } },
        {
          $project: {
            data: { $slice: ['$data', offset, limit] },
            count: '$count',
            limit: { $literal: limit },
            offset: { $literal: offset }
          }
        }
      ]).allowDiskUse(true);
      logger.success(`Result: ${result.length < 1 ? 0 : result[0].data.length} record/s found`);

      if (result.length < 1) { return nulldata };

      const history = result[0];
      for (let i = 0; i < history.data.length; i++) {
        const type = history.data[i].type.name;


        const author = !history.data[i].author ? 'Unknown User' : `${history.data[i].author.firstName} ${history.data[i].author.lastName}`;
        if (history.data[i].action === exchange.registry.RECORD_CREATED)
          history.data[i].activity = `${author} created this item`;
        else if (history.data[i].action === exchange.registry.RECORD_UPDATED) {
          if (!history.data[i].changes)
            history.data[i].activity = `${author} edited this item`;
          else if (history.data[i].changes.status === status.registry.ACTIVE)
            history.data[i].activity = `${author} published this item`;
          else if (history.data[i].changes.status === status.registry.UNPUBLISHED)
            history.data[i].activity = `${author} unpublished this item`;
          else if (history.data[i].changes.status === status.registry.SCHEDULED)
            history.data[i].activity = `${author} scheduled this item`;
          else if (history.data[i].changes.status === status.registry.TRASHED)
            history.data[i].activity = `${author} moved this item to trash`
          else if (history.data[i].changes.status === status.registry.CLAIMED)
            history.data[i].activity = `${author} tagged this item as claimed`
          else if (history.data[i].changes.status === status.registry.RECEIVED)
            history.data[i].activity = `${author} tagged this item as received`
          else
            history.data[i].activity = `${author} edited this item`;
        }
      }
      return history;

    } catch (error) {
      logger.error(error)
      ctx.res.status(error.status || 500);
      throw error;
    }
  },

};

module.exports = {
  Query
};