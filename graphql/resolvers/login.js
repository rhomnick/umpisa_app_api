const { Log, EventPublisher, validate, ResolverError, errorCodes, encrypt, amqp, constants } = require('../../helpers');
const httpStatus = require('http-status-codes');
const Auth = require('../auth')

const Mutation = {
  async login(_, args, ctx, info) {
    const log = new Log(ctx.processId, info.fieldName);
    log.init(ctx.headers, args);

    try {
      const { validators, models } = ctx;
      const { Users, Records } = models;
      const { LoginSchema } = validators;
      const { password, email } = args;

      validate(log, LoginSchema, args);

      const user = await Users.findOne({ email: new RegExp(email, 'i'), status: 'active' })
        .then(doc => {
          if (!doc || !encrypt.compare(password, doc.password)) {
            log.error(`${errorCodes[1002]}`);
            throw new ResolverError(errorCodes[1002], 1002, httpStatus.BAD_REQUEST);
          }

          log.info('Fetching user -> [OK]');
          return doc;
        });

      const token = await Auth.sign({
        processId: log.processId,
        user: {
          _id: user._id,
          email: user.email,
        },
        device: ctx.headers.treat,
        spoil: ctx.headers.spoil
      }).then(result => {
        if (result.error) {
          const error = result.error;
          throw new ResolverError(error.message, error.code, httpStatus.UNAUTHORIZED);
        }
        return result.token;
      });

      ctx.res.set('slave', token);

      let analytics = await Records
        .aggregate()
        .group({ _id: "$typeLiteral", count: { $sum: 1 } })
        .sort("-count")
        .limit(1000).allowDiskUse(true);
      let analyticsObj = {}
      let _analytics = analytics.map((item) => {
        analyticsObj[item._id] = item.count
      })
      user.analytics = analyticsObj
      const responseObj = {
        user: user,
        slave: token
      };

      log.success(JSON.stringify(responseObj));

      return responseObj;
    } catch (error) {
      if (error.code) {
        ctx.res.status(error.status);
        throw error;
      } else {
        ctx.res.status(httpStatus.INTERNAL_SERVER_ERROR);
        log.error(error.message);
        throw new ResolverError(errorCodes[3000], 3000);
      }
    }
  },
};

const Query = {
  async profile(_, args, ctx, info) {
    const log = new Log(ctx.processId, info.fieldName);
    log.init(ctx.headers, args);

    try {
      const { validators, models } = ctx;
      const { Users, Records } = models;

      // validate(log, LoginSchema, args);

      const user = await Users.findOne({ email: ctx.user.email })
        .then(doc => {
          log.info('Fetching user -> [OK]');
          return doc;
        });
      let analytics = await Records
        .aggregate()
        .group({ _id: "$typeLiteral", count: { $sum: 1 } })
        .sort("-count")
        .limit(1000).allowDiskUse(true);
      let analyticsObj = {}
      let _analytics = analytics.map((item) => {
        analyticsObj[item._id] = item.count
      })
      user.analytics = analyticsObj
      log.success(JSON.stringify(user));

      return user;
    } catch (error) {
      if (error.code) {
        ctx.res.status(error.status);
        throw error;
      } else {
        ctx.res.status(httpStatus.INTERNAL_SERVER_ERROR);
        log.error(error.message);
        throw new ResolverError(errorCodes[3000], 3000);
      }
    }
  }
}

module.exports = {
  Mutation,
  Query
};
