const httpStatus = require('http-status-codes');
const { Log, validate, ResolverError, errorCodes, encrypt, constants } = require('../../helpers');
const { events, service, status } = constants;
// const {  } = commons;

const Mutation = {

  async createType(_, args, ctx, info) {
    const log = new Log(ctx.processId, info.fieldName);
    log.init(ctx.headers, args);

    try {
      const { Types } = ctx.models;
      const { CreateTypeSchema } = ctx.validators;

      validate(log, CreateTypeSchema, args.data);

      args.data.authorId = ctx.user._id;

      const exist = await Types.findOne({ name: args.data.name });
      if (exist)
        throw new ResolverError(' type name already exists.', 1004, httpStatus.BAD_REQUEST)

      const result = await new Types(args.data).save();
      log.info('Creating Type -> [OK]')
      // *****  Send Data to EventStore
      const { createdAt, updatedAt, ...body } = result._doc;

      // ***** Response Data
      const responseObj = {
        _id: result._id,
        message: 'success',
        processId: ctx.processId
      };
      log.success(JSON.stringify(responseObj));
      return responseObj;

    } catch (error) {
      if (error.code) {
        ctx.res.status(error.status);
        throw error;
      } else {
        ctx.res.status(httpStatus.INTERNAL_SERVER_ERROR);
        log.error(error);
        throw new ResolverError(errorCodes[3000] + ` (${ctx.processId})`, 3000);
      }
    }
  },

  async updateType(_, args, ctx, info) {
    const log = new Log(ctx.processId, info.fieldName);
    log.init(ctx.headers, args);

    try {
      const { Types } = ctx.models;
      const { UpdateTypeSchema } = ctx.validators;

      validate(log, UpdateTypeSchema, args);

      const result = await Types.findOne({ _id: args.id });
      if (!result)
        throw new ResolverError(' type not found.', 5000, httpStatus.NOT_FOUND)


      // ***** Update Data
      await result.set(args.data);
      await result.save();
      log.info('Updating Type -> [OK]')
      // *****  Send Data to EventStore
      const { createdAt, updatedAt, ...body } = result._doc;

      // ***** Response Data
      const responseObj = {
        _id: result._id,
        message: 'success',
        processId: ctx.processId
      };
      return responseObj;

    } catch (error) {
      if (error.code) {
        ctx.res.status(error.status);
        throw error;
      } else {
        ctx.res.status(httpStatus.INTERNAL_SERVER_ERROR);
        log.error(error);
        throw new ResolverError(errorCodes[3000] + ` (${ctx.processId})`, 3000);
      }
    }
  },

};

module.exports = {
  Mutation
};
