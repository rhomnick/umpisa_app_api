const { Log, EventPublisher, validate, ResolverError, errorCodes, encrypt, amqp, constants } = require('../../helpers');
const httpStatus = require('http-status-codes');

const Mutation = {
  async logout(_, args, ctx, info) {
    const log = new Log(ctx.processId, info.fieldName);
    log.init(ctx.headers, args);

    try {
      const { validators } = ctx;
      const { slave, spoil, treat } = ctx.headers;
      const { data } = args;
      const { LogoutSchema } = validators;

      validate(log, LogoutSchema, args);

      await amqp.request('delete', {
        processId: log.processId,
        token: slave,
        device: treat,
        spoil
      }).then(result => {
        if (result.error) {
          const error = result.error;
          log.error(errorCodes[error.code]);
          throw new ResolverError(error.message, error.code, httpStatus.UNAUTHORIZED);
        }

        log.info(`Deleting user to auth -> [OK]`);
      });

      if (data && data.notificationToken) {
        await amqp.send('DELETE_NOTIFICATION_TOKEN', {
          processId: log.processId,
          headers: ctx.headers,
          data: {
            notificationToken: data.notificationToken
          }
        }, { durable: true, presistent: true });
      }

      const responseObj = {
        _id: ctx.user.id,
        message: 'success'
      };

      log.success(responseObj);
      return responseObj;
    } catch (error) {
      if (error.code) {
        ctx.res.status(error.status);
        throw error;
      } else {
        ctx.res.status(httpStatus.INTERNAL_SERVER_ERROR);
        log.error(error.message);
        throw new ResolverError(errorCodes[3000], 3000);
      }
    }
  }
};

module.exports = {
  Mutation
};
