const httpStatus = require('http-status-codes');
const { Log, validate, ResolverError, errorCodes, encrypt, constants } = require('../../helpers');
const { events, service, status } = constants;

const Mutation = {


  async createCategory(_, args, ctx, info) {
    const log = new Log(ctx.processId, info.fieldName);
    log.init(ctx.headers, args);

    try {
      const { Categories } = ctx.models;
      const { CreateCategorySchema } = ctx.validators;

      validate(log, CreateCategorySchema, args.data);
      args.data.authorId = ctx.user._id;

      const exist = await Categories.findOne({ name: args.data.name });
      if (exist)
        throw new ResolverError('Type name already exists.', 1004, httpStatus.BAD_REQUEST)

      const result = await new Categories(args.data).save();
      log.info('Creating Category -> [OK]')

      // ***** Response Data
      const responseObj = {
        _id: result._id,
        message: 'success',
        processId: ctx.processId
      };
      log.success(JSON.stringify(responseObj));
      return responseObj;

    } catch (error) {
      if (error.code) {
        ctx.res.status(error.status);
        throw error;
      } else {
        ctx.res.status(httpStatus.INTERNAL_SERVER_ERROR);
        log.error(error);
        throw new ResolverError(errorCodes[3000] + ` (${ctx.processId})`, 3000);
      }
    }
  },

  async updateCategory(_, args, ctx, info) {
    const log = new Log(ctx.processId, info.fieldName);
    log.init(ctx.headers, args);

    try {
      const { Categories } = ctx.models;
      const { UpdateCategorySchema } = ctx.validators;

      validate(log, UpdateCategorySchema, args);

      const result = await Categories.findOne({ _id: args.id });
      if (!result)
        throw new ResolverError('Category not found.', 5000, httpStatus.NOT_FOUND)


      // ***** Update Data
      await result.set(args.data);
      await result.save();
      log.info('Updating Category -> [OK]')
      // *****  Send Data to EventStore
      const { createdAt, updatedAt, ...body } = result._doc;

      // ***** Response Data
      const responseObj = {
        _id: result._id,
        message: 'success',
        processId: ctx.processId
      };
      log.success(JSON.stringify(responseObj));
      return responseObj;

    } catch (error) {
      if (error.code) {
        ctx.res.status(error.status);
        throw error;
      } else {
        ctx.res.status(httpStatus.INTERNAL_SERVER_ERROR);
        log.error(error);
        throw new ResolverError(errorCodes[3000] + ` (${ctx.processId})`, 3000);
      }
    }
  },

};

module.exports = {
  Mutation
};
