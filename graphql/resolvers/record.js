const httpStatus = require('http-status-codes');
const { Log, validate, ResolverError, errorCodes, encrypt, constants } = require('../../helpers');
const { events, service, status } = constants;

const Mutation = {

  async createRecord(_, args, ctx, info) {
    const log = new Log(ctx.processId, info.fieldName);
    log.init(ctx.headers, args);

    try {
      const { Categories, Types, Records, Notes } = ctx.models;
      const { CreateRecordSchema } = ctx.validators;

      validate(log, CreateRecordSchema, args.data);
      args.data.authorId = ctx.user._id;

      // Check wether the ids are existing
      // const type_exist = await Types.findOne({ _id: args.data.typeId });
      // if (!type_exist)
      //   throw new ResolverError(' type does not exist.', 5000, httpStatus.NOT_FOUND)
      // const cat_exist = await Categories.findOne({ _id: args.data.categoryId });
      // if (!cat_exist)
      //   throw new ResolverError(' category does not exist.', 5000, httpStatus.NOT_FOUND)

      const result = await new Records(args.data).save();
      log.info('Creating Record -> [OK]')


      //  *** send initial note
      if (args.note) {
        const _result = await new Notes({
          content: args.note,
          recordId: result._id,
          authorId: ctx.user.accountId
        }).save();
        const { createdAt, updatedAt, ...body } = _result._doc;

      }

      // ***** Response Data
      const responseObj = {
        _id: result._id,
        message: 'success',
        processId: ctx.processId
      };
      log.success(JSON.stringify(responseObj));
      return result;

    } catch (error) {
      if (error.code) {
        ctx.res.status(error.status);
        throw error;
      } else {
        ctx.res.status(httpStatus.INTERNAL_SERVER_ERROR);
        log.error(error);
        throw new ResolverError(errorCodes[3000] + ` (${ctx.processId})`, 3000);
      }
    }
  },

  async updateRecord(_, args, ctx, info) {
    const log = new Log(ctx.processId, info.fieldName);
    log.init(ctx.headers, args);

    try {
      const { Records } = ctx.models;
      const { UpdateRecordSchema } = ctx.validators;

      validate(log, UpdateRecordSchema, args);

      const result = await Records.findOne({ _id: args.id });
      if (!result)
        throw new ResolverError(' record not found.', 5000, httpStatus.NOT_FOUND)


      // ***** Update Data
      await result.set(args.data);
      await result.save();
      log.info('Updating Record -> [OK]')

      // ***** Response Data
      const responseObj = {
        _id: result._id,
        message: 'success',
        processId: ctx.processId
      };
      log.success(JSON.stringify(responseObj));
      return responseObj;

    } catch (error) {
      if (error.code) {
        ctx.res.status(error.status);
        throw error;
      } else {
        ctx.res.status(httpStatus.INTERNAL_SERVER_ERROR);
        log.error(error);
        throw new ResolverError(errorCodes[3000] + ` (${ctx.processId})`, 3000);
      }
    }
  },

};

module.exports = {
  Mutation
};
