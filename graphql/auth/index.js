const { Log, ResolverError, errorCodes } = require('../../helpers');
const moment = require('moment');
const { ObjectID } = require('mongodb');
const { Session, Role, Setting } = require('mongoose').models;

const { key, algorithm } = require('../../config').jwt;
const jwt = require('jsonwebtoken');

const sign = async payload => {
  return await jwt.sign(payload, key, { algorithm });
}
const verify = async token => {
  return await jwt.verify(token, key);
}

const verifyTime = timestamp => {
  try {
    const currentTime = moment();
    const behindBorder = currentTime.subtract(5, 'minutes');
    const advanceBorder = currentTime.add(5, 'minutes');

    const requestTime = moment(new Date(timestamp));

    const isBehind = requestTime < behindBorder;
    const isAdvance = requestTime > advanceBorder;

    if (isBehind || isAdvance) {
      throw new Error(`device time is hacked!`);
    }
  } catch (error) {
    throw error;
  }
};

module.exports = {
  async sign(msg) {
    const { processId, spoil, ...data } = msg;

    try {
      // verify token
      verifyTime(spoil);

      const token = await sign(data);

      const currentSession = await Session.findOne({
        device: data.device,
        'user._id': data.user._id
      });

      if (currentSession) {
        const responseObj = {
          token: currentSession.token
        };


        return responseObj;
      } else {

        const result = await Session.create({
          ...data,
          token,
          createdAt: new Date()
        });

        const responseObj = {
          token: result.token
        };


        return responseObj;
      }
    } catch (error) {
      return {
        error: {
          message: error.message,
          code: error.code
        }
      };
    }
  },

  async verify(msg) {
    const { processId, token, spoil, device, fieldName } = msg;

    try {
      // verify session
      verifyTime(spoil);

      const deviceSession = await Session.findOne({
        token: token,
        device: device
      });

      if (!deviceSession) {
        throw new Error('invalid session');
      }

      const decoded = await verify(token);

      const session = await Session.findOne({
        token: token,
        'user._id': decoded.user._id,
        'user.email': decoded.user.email,
        device: decoded.device
      });

      if (!session) {
        throw new Error('invalid session');
      }

     

      const responseObj = {
        user: session.user
      };


      return responseObj;
    } catch (error) {

      if (error.name === 'JsonWebTokenError') {
        return {
          error: 'Unathorized Request'
        };
      }

      return {
        error: error.message
      };
    }
  },

  async delete(msg) {
    const { processId, token, spoil, device } = msg;

    try {
      verifyTime(spoil);

      const deviceSession = await Session.findOne({
        token: token,
        device: device
      });

      if (!deviceSession) {
        // log.error(errorCodes[2002]);
        throw new ResolverError(errorCodes[2000], 2002);
      }

      const deleteResult = await Session.deleteOne({ _id: deviceSession._id });

      const responseObj = {
        result: deleteResult
      };


      return responseObj;
    } catch (error) {
      return {
        error: {
          message: error.message,
          code: error.code
        }
      };
    }
  },

  async findRole(msg) {
    const { processId, query } = msg;


    try {
      const role = await Role.findOne(query);
      return role;
    } catch (error) {

      if (error.name === 'JsonWebTokenError') {
        return {
          error: 'Unathorized Request'
        };
      }

      return {
        error: error.message
      };
    }
  }
}
