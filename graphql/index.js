const { graphqlExpress } = require('apollo-server-express');
const { makeExecutableSchema } = require('graphql-tools');
const path = require('path');
const { fileLoader, mergeTypes, mergeResolvers } = require('merge-graphql-schemas');
const lodash = require('lodash');

const mongoose = require('mongoose');
const validators = require('../validators');

const { directives } = require('../helpers');

const schema = makeExecutableSchema({
  // Merge this typedefs instead if you want to compile API docs.
  // typeDefs: mergeTypes(fileLoader(path.join(__dirname, './docs'))),
  typeDefs: mergeTypes(fileLoader(path.join(__dirname, './types'))),
  resolvers: mergeResolvers(fileLoader(path.join(__dirname, './resolvers'))),
  directiveResolvers: directives()
});

const graphql = graphqlExpress((req, res) => ({
  schema: schema,
  context: {
    headers: req.headers,
    models: mongoose.models,
    validators,
    res,
    req,
    lodash
  },
  formatError(err) {
    return {
      message: err.message,
      code: err.originalError && err.originalError.code,
      locations: err.locations,
      path: err.path
    };
  }
}));

module.exports = graphql;
