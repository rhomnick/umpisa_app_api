const { ObjectId } = require('mongodb');

const data = [
  {
    _id: ObjectId("5d7ceb2f7c42de295183fbf8"),
    firstName: "Rhomnick",
    lastName: "Coloma",
    avatar: "https://pixel.nymag.com/imgs/fashion/daily/2019/06/18/18-puppy-dog-eyes.w700.h467.2x.jpg",
    email: "nick@email.com",
    password: "$2a$10$sP.fYl4ue6onrEpBqDhX6.ji/LAe3sIrvUV6R6oMklohQhVOM3WOK",
    userType: null,
    status: "active",
  }
];

module.exports = data;