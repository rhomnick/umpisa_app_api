const { ObjectId } = require('mongodb');

const data = [
  {
    _id: ObjectId("5d7cfca77c42de295183fc0b"),
    status: "active",
    publishedAt: new Date(),
    typeLiteral: "posts",
    title: "Plastic Problem",
    content: "Americans use billions of plastic water bottles every year, most of which end up in a landfill. The LARQ bottle is like many of the reusable bottle alternatives, but with one big standout feature: Using UV light",
    authorId: ObjectId("5d7ceb2f7c42de295183fbf8"),
    updatedAt: new Date(),
  },

  {
    _id: ObjectId("5d7cfc877c42de295183fc0a"),
    status: "active",
    publishedAt: new Date(),
    typeLiteral: "posts",
    title: "AllBirds SweetFoam",
    content: "The global footwear industry has a big carbon footprint, as companies burn energy manufacturing and transporting their product and use petroleum to make components like rubber soles",
    authorId: ObjectId("5d7ceb2f7c42de295183fbf8"),
    updatedAt: new Date(),
  },

  {
    _id: ObjectId("5d7cfc707c42de295183fc09"),
    status: "active",
    publishedAt: new Date(),
    typeLiteral: "posts",
    title: "Just Egg",
    content: "The animal agriculture industry is responsible for around 15% of the world’s greenhouse gas emissions, according to a U.N. estimate, thanks to the immense resources required to raise and feed animals.",
    authorId: ObjectId("5d7ceb2f7c42de295183fbf8"),
    updatedAt: new Date(),
  },

  {
    _id: ObjectId("5d7cfb067c42de295183fc03"),
    status: "active",
    publishedAt: new Date(),
    typeLiteral: "products",
    title: "Bird and Lime Bike",
    primaryImage: "https://timedotcom.files.wordpress.com/2018/11/lime-sustainable-product.jpg",
    content: "The sharable electric scooters that took cities across America by storm this year promise to serve as a more eco-friendly way for city dwellers to quickly get from A to B. As urban planners and policymakers look for alternatives to automobiles in crowded downtown areas, they present a promising solution.",
    authorId: ObjectId("5d7ceb2f7c42de295183fbf8"),
    updatedAt: new Date(),
  },

  {
    _id: ObjectId("5d7cfa7c7c42de295183fc02"),
    status: "active",
    publishedAt: new Date(),
    typeLiteral: "products",
    title: "LARQ Bottles",
    primaryImage: "https://timedotcom.files.wordpress.com/2018/11/larq-sustainable-product.jpg",
    content: "Americans use billions of plastic water bottles every year, most of which end up in a landfill. The LARQ bottle is like many of the reusable bottle alternatives, but with one big standout feature: Using UV light, it cleans both water kept inside the bottle, and the inside surface of the bottle itself.",
    authorId: ObjectId("5d7ceb2f7c42de295183fbf8"),
    updatedAt: new Date(),
  },

  {
    _id: ObjectId("5d7cf9637c42de295183fc01"),
    status: "active",
    publishedAt: new Date(),
    typeLiteral: "products",
    title: "Just Egg",
    primaryImage: "https://timedotcom.files.wordpress.com/2018/11/just-egg-sandwich-sustainable-product.jpg",
    content: "The animal agriculture industry is responsible for around 15% of the world’s greenhouse gas emissions, according to a U.N. estimate, thanks to the immense resources required to raise and feed animals.",
    authorId: ObjectId("5d7ceb2f7c42de295183fbf8"),
    updatedAt: new Date(),
  },

  {
    _id: ObjectId("5d7cf9047c42de295183fc00"),
    status: "active",
    publishedAt: new Date(),
    typeLiteral: "products",
    title: "AllBirds SweetFoam",
    primaryImage: "https://timedotcom.files.wordpress.com/2018/11/sweetfoam-sustainable-product.jpg",
    content: "The global footwear industry has a big carbon footprint, as companies burn energy manufacturing and transporting their product and use petroleum to make components like rubber soles",
    authorId: ObjectId("5d7ceb2f7c42de295183fbf8"),
    updatedAt: new Date(),
  },

  {
    _id: ObjectId("5d7cf8267c42de295183fbff"),
    status: "active",
    publishedAt: new Date(),
    typeLiteral: "products",
    title: "Maybelline Great Lash Mascara",
    primaryImage: "https://scstylecaster.files.wordpress.com/2018/11/maybelline-great-lash-mascara-amazon.jpg",
    content: "",
    authorId: ObjectId("5d7ceb2f7c42de295183fbf8"),
    updatedAt: new Date(),
  },

  {
    _id: ObjectId("5d7cf77e7c42de295183fbfe"),
    status: "active",
    publishedAt: new Date(),
    typeLiteral: "products",
    title: "6 Old-School Beauty Products We Still Swear by for Good Reason",
    primaryImage: "https://scstylecaster.files.wordpress.com/2018/11/old-school-products.jpg",
    content: "The beauty world is in the midst of a major pop-culture moment and arguably undergoing a huge transformation.",
    authorId: ObjectId("5d7ceb2f7c42de295183fbf8"),
    updatedAt: new Date(),
  },

  {
    _id: ObjectId("5d7cf6b97c42de295183fbfd"),
    status: "active",
    publishedAt: new Date(),
    typeLiteral: "posts",
    title: "30 Minute Meals",
    content: "You've heard of a makeover, no doubt, but have you heard of a man-over (AKA a makeover for a man)? Well, Rachael surprised her long-haired culinary production assistant, Max, with a 30-minute makeover in honor of the return of 30 Minute Meals. ",
    authorId: ObjectId("5d7ceb2f7c42de295183fbf8"),
    updatedAt: new Date(),
  },

  {
    _id: ObjectId("5d7cf6797c42de295183fbfc"),
    status: "active",
    publishedAt: new Date(),
    typeLiteral: "posts",
    title: "The Micro-Workout You Never Knew You Needed",
    content: "Okay, we all know you can work out your arms, legs, abs and even your butt — but did you know you could work out your toes and even the inside of your mouth?! Well, it's 2019, and you CAN. And they're called micro-workouts. Now, micro-workouts can refer to high-intensity workouts done in short periods of time, but in this case",
    authorId: ObjectId("5d7ceb2f7c42de295183fbf8"),
    updatedAt: new Date(),
  },

  {
    _id: ObjectId("5d7cecef7c42de295183fbfb"),
    status: "active",
    publishedAt: new Date(),
    typeLiteral: "posts",
    title: "Prevent Your Dog From Overheating",
    content: "At LONG last, friends, its almost the warmest time of the year! But while summer may have most of us rejoicing, it should also make those of us with pets — especially dogs who go outside — extremely cautious, as it’s all too easy for our four-legged friends to overheat.",
    authorId: ObjectId("5d7ceb2f7c42de295183fbf8"),
    updatedAt: new Date(),
  }
];

module.exports = data;