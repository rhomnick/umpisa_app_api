const { Seeder } = require('mongo-seeding');
const path = require('path');

const config = {
  database: {
    host: '127.0.0.1',
    port: 27016,
    name: 'umpisa',
  },
  dropDatabase: true,
};

const seeder = new Seeder(config);
const collections = seeder.readCollectionsFromPath(
  path.resolve("./mongo/seeds"),
  {
    transformers: [Seeder.Transformers.replaceDocumentIdWithUnderscoreId],
  });

seeder
  .import(collections)
  .then(() => {
    console.log('Planting Seeds Success')
  })
  .catch(err => {
    console.log('Failed', err)
  });