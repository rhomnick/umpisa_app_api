const validator = require('./validator');

const schema = {
  data: {
    type: 'object',
    props: {
      avatar: {
        type: 'string',
        optional: true,
        empty: false
      },
      email: {
        type: 'string',
        optional: false,
        empty: false
      },
      firstName: {
        type: 'string',
        optional: false,
        empty: false
      },
      lastName: {
        type: 'string',
        optional: false,
        empty: false
      },
      password: {
        type: 'string',
        optional: false,
        empty: false
      }
    }
  }
};

module.exports = validator.compile(schema);
