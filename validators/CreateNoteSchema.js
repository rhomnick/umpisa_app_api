const validator = require('./validator');

const schema = {
    recordId: { type: 'string', empty: false },
    content: { type: 'string', empty: false },
};

module.exports = validator.compile(schema);