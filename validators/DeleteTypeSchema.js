const validator = require('./validator');

const schema = {
    name: { type: 'string', empty: false },
    status: { type: 'string', empty: false, optional: true}
};

module.exports = validator.compile(schema);
