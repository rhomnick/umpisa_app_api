const Validator = require('fastest-validator');

let validator = new Validator({
    messages: {
    // Register our new error message text
    ObjectIdErr: "The '{field}' field must be a valid ObjectId"
    }
});

// Create Custom Validation (For Mongoose ObjectID)
validator.add("objectId", value => {
    if(/^[0-9a-fA-F]{24}$/.test(value))
        return true
    else    
        return validator.makeError("ObjectIdErr", null, value);
});

module.exports = validator;
