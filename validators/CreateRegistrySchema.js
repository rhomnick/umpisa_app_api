const validator = require('./validator');

const schema = {
    author: {type: 'objectId', optional: true},
    category: { type: 'objectId', optional: true }, // this is optional because of forms.
    subcategory: { type: 'objectId', optional: true },
    title: { type: 'string', empty: false },
    type: { type: 'string', empty: false, optional: true},
    primaryMedia: {
      type: 'array',
      items: 'object',
      props: {
        url: { type: 'string', empty: false },
        type: { type: 'string', empty: false }
      }
    },
    content: { type: 'string', empty: false, optional: true },
    audienceExpanse: {
      type: 'object',
      optional: true,
      props: {
        organizationIds: { type: 'array', items: 'objectId', empty: false, optional: true },
        branchIds: { type: 'array', items: 'objectId', empty: false, optional: true },
        departmentIds: { type: 'array', items: 'objectId', empty: false, optional: true },
        teamIds: { type: 'array', items: 'objectId', empty: false, optional: true },
        userIds: { type: 'array', items: 'objectId', empty: false, optional: true }
      }
    },
    audienceType: { type: 'string', empty: false },
    audienceExceptions: {
      type: 'object',
      optional: true,
      empty: false,
      props: {
        organizationIds: { type: 'array', items: 'objectId', empty: false, optional: true },
        branchIds: { type: 'array', items: 'objectId', empty: false, optional: true },
        departmentIds: { type: 'array', items: 'objectId', empty: false, optional: true },
        teamIds: { type: 'array', items: 'objectId', empty: false, optional: true },
        userIds: { type: 'array', items: 'objectId', empty: false, optional: true }
      }
    },
    publishedAt: { type: 'string', optional: true },
    sponsor: {
      type: 'object',
      optional: true,
      props: {
        name: { type: 'string', empty: false },
        imageUrl: { type: 'string', empty: false }
      }
    },
    status: { type: 'string', empty: false, optional: true}
};

module.exports = validator.compile(schema);
