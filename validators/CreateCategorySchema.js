const validator = require('./validator');

const schema = {
    name: { type: 'string', empty: false },
    status: { type: 'string', empty: false, optional: true},
    typeId: { type: 'objectId' }
};

module.exports = validator.compile(schema);
