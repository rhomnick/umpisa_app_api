const validator = require('./validator');

const schema = {
  data: {
    type: 'object',
    optional: true,
    notificationToken: {
      type: 'string',
      optional: true,
      empty: false
    }
  }
};

module.exports = validator.compile(schema);