const validator = require('./validator');

const schema = {
    title: { type: 'string', empty: false, optional: false },
    primaryImage: { type: 'string', empty: false, optional: true },
    content: { type: 'string', empty: false, optional: false },
    typeId: { type: 'objectId', empty: false, optional: true },
    typeLiteral: { type: 'string', empty: false, optional: true },
    categoryId: { type: 'objectId', empty: false, optional: true },
};

module.exports = validator.compile(schema);
