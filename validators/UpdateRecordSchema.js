const validator = require('./validator');

const schema = {
    id: { type: 'objectId' },
    data: {
        type: 'object',
        props: {
            status: { type: 'string', empty: false, optional: true },
            typeId: { type: 'objectId', empty: false, optional: true },
            categoryId: { type: 'objectId', empty: false, optional: true },
            checkInSchedule: { type: 'string', empty: false, optional: true },
            checkedInAt: { type: 'string', empty: false, optional: true },
            checkedOutAt: { type: 'string', empty: false, optional: true },
            claimedAt: { type: 'string', empty: false, optional: true },
            claimantId: { type: 'objectId', empty: false, optional: true },
            forWhoId: { type: 'objectId', empty: false, optional: true },
            forWhatId: { type: 'objectId', empty: false, optional: true },

            visitor: {
                type: 'object',
                optional: true,
                props: {
                    firstName: { type: 'string', empty: false, optional: true },
                    lastName: { type: 'string', empty: false, optional: true },
                    company: { type: 'string', empty: false, optional: true },
                }
            }
        }
    }
};

module.exports = validator.compile(schema);
