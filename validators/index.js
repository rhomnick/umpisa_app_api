module.exports = {
  RegisterSchema: require('./RegisterSchema'),
  LoginSchema: require('./LoginSchema'),
  LogoutSchema: require('./LogoutSchema'),

  CreateTypeSchema: require('./CreateTypeSchema'),
  UpdateTypeSchema: require('./UpdateTypeSchema'),
  DeleteTypeSchema: require('./DeleteTypeSchema'),

  CreateCategorySchema: require('./CreateCategorySchema'),
  UpdateCategorySchema: require('./UpdateCategorySchema'),
  // DeleteCategorySchema: require('./DeleteCategorySchema'),

  CreateRecordSchema: require('./CreateRecordSchema'),
  UpdateRecordSchema: require('./UpdateRecordSchema'),
  // DeleteRecordSchema: require('./DeleteRecordSchema'),

  CreateNoteSchema: require('./CreateNoteSchema'),
  UpdateNoteSchema: require('./UpdateNoteSchema'),

  // CreateSchema: require('./CreateSchema'),

};