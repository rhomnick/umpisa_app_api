const validator = require('./validator');

const schema = {
  email: {
    type: 'string',
    optional: false,
    empty: false
  },
  password: {
    type: 'string',
    optional: false,
    empty: false
  }
};

module.exports = validator.compile(schema);
