const validator = require('./validator');

const schema = {
    id: { type: 'objectId' },
    data: {
        type: 'object',
        props: {
            name: { type: 'string', empty: false, optional: true },
            status: { type: 'string', empty: false, optional: true }
        }
    }
};

module.exports = validator.compile(schema);
