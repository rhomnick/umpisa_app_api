require('./models');
require('./helpers');

const { models } = require('mongoose');
const helmet = require('helmet');

const Server = require('abtool-server');
const config = require('./config');

const graphql = require('./graphql');

const server = new Server(config.port, config.host, {
  cors: config.cors
});

const app = server.getApp();

app.use(helmet.referrerPolicy());
app.use(helmet.hidePoweredBy());
app.use('/api', graphql);
app.use('/health', (req, res) => {
  res.sendStatus(200)
});

server.start(() => {
  console.log(`Umpisa-API server is now running on ${server.host}:${server.port}`);
});